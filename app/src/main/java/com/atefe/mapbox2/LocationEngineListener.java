package com.atefe.mapbox2;

import android.location.Location;
import android.os.Bundle;

public interface LocationEngineListener {

    public abstract void onLocationChanged (Location location);
    public void onConnected();
}
